/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ReceiptDao;
import com.werapan.databaseproject.dao.ReceiptDetailDao;
import com.werapan.databaseproject.model.Receipt;
import com.werapan.databaseproject.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptService {
    public Receipt getById(int id){
        ReceiptDao receiptDao =new ReceiptDao();
        return  receiptDao.get(id);
    }
    
    public List<Receipt> getReciepts(){
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" receipt_id asc");
    }

    public Receipt addNew(Receipt editedReciept) {
        ReceiptDao receiptDao = new ReceiptDao();
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        Receipt receipt = receiptDao.save(editedReciept);
        for(ReceiptDetail rd : editedReciept.getReceiptDetails()){
            rd.setReceiptId(receipt.getId());
            receiptDetailDao.save(rd);
        }
        return receipt;
    }

    public Receipt update(Receipt editedReciept) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReciept);
    }

    public int delete(Receipt editedReciept) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReciept);
    }
}
